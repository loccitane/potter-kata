import console.Console
import kata.KataInformation
import java.awt.Color

fun main(args: Array<String>) {

    Console.println("Starting Potter Kata Pricing App\n", Color.BLUE)

    Console.println("Kata description:")
    Console.println(KataInformation.description, Color.GREEN)

    Console.println("Basket example:")
    Console.println(KataInformation.sampleProblem, Color.CYAN)

    Console.println("Example pricing:")
    Console.println(KataInformation.sampleResult, Color.CYAN)
}