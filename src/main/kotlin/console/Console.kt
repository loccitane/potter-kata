package console

import java.awt.Color

class Console {
    companion object
    {
        private val colorMapping = hashMapOf(
            Color.RED to ANSI_RED,
            Color.GREEN to ANSI_GREEN,
            Color.YELLOW to ANSI_YELLOW,
            Color.BLUE to ANSI_BLUE,
            Color.CYAN to ANSI_CYAN,
            Color.BLACK to ANSI_BLACK
        )

        fun println(message: String, color: Color = Color.GRAY){
            val ansiCode = mapColorToAnsiCode(color)
            kotlin.io.println( ansiCode + message + ANSI_RESET)
        }

        fun printColorSample() {
            println("Color Test:")
            colorMapping.forEach { color -> println(color.value + " -> Testing ${color.key} console output." + ANSI_RESET) }
            println("-----------\n")
        }

        private fun mapColorToAnsiCode(color: Color): String {
            return colorMapping[color] ?: ANSI_GREY
        }
    }
}