package kata

class KataInformation {

    companion object {
        const val description: String =
                "Once upon a time there was a series of 5 books about a very English hero called Harry.\n"+
                "Children all over the world thought he was fantastic, and, of course, so did the publisher.\n"+
                "So in a gesture of immense generosity to mankind, they set up the following pricing model\n"+
                "to take advantage of Harry’s magical powers. One copy of any of the five books costs 8 EUR.\n"+
                "If, however, you buy two different books from the series, you get a 5% discount on those two books.\n" +
                "If you buy 3 different books, you get a 10% discount. With 4 different books, you get a 20% discount.\n" +
                "If you go the whole hog, and buy all 5, you get a huge 25% discount. Note that if you buy, say, four books,\n" +
                "of which 3 are different titles, you get a 10% discount on the 3 that form part of a set, but the fourth book\n" +
                "still costs 8 EUR. Potter mania is sweeping the country and parents of teenagers everywhere are queueing up\n" +
                "with shopping baskets overflowing with Potter books. Your mission is to write a piece of code to calculate\n" +
                "the price of any conceivable shopping basket, giving as big a discount as possible.\n"

        const val sampleProblem: String= "How much does this basket of books cost?\n" +
                "\t- 2 copies of the first book\n" +
                "\t- 2 copies of the second book\n" +
                "\t- 2 copies of the third book\n" +
                "\t- 1 copy of the fourth book\n" +
                "\t- 1 copy of the fifth book\n"

        const val sampleResult: String = "\t  (4 * 8) - 20% [first book, second book, third book, fourth book]\n" +
        "\t+ (4 * 8) - 20% [first book, second book, third book, fifth book]\n" +
        "\t= 25.6 * 2\n" +
        "\t= 51.20\n"
    }
}
